﻿using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.WebApi.Models;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Web.Http;

namespace com.mbds.haiti.immoinfo.WebApi.Controllers
{

    public class EncheresController : ApiController
    {
        // GET api/<controller>/5
        [HttpGet,Route("api/encheres/{statut}")]
        //[ValidateModelState]
        [SwaggerOperation("ListeEncheresParStatut")]
        public List<Enchere> ListeEncheresParStatut( StatutEnchere statut)
        {
            List<Enchere> lstench = new List<Enchere>();
            TbEnchereDAOImpl enchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> ListEnchere = enchereDAOImpl.ListerEnchereParStatut((int)(StatutEnchere)statut);
             foreach (var child in ListEnchere)
             {
                 var enumDisplayStatus = (StatutEnchere)child.Statut;
                 string valeurEnum = enumDisplayStatus.ToString();
                 lstench.Add(new Enchere(child.IdEncheres, 
                                         child.CommentiareValidation,
                                         child.DateCloture, 
                                         child.DateCommandeDeCloture,
                                         child.DateOuverture,
                                         child.MontatDepart,
                                         valeurEnum,
                                         child.Propriete));
             }
             
            return lstench;

        }

        // GET api/<controller>
        [HttpGet,Route ("api/encheres")]
       // [ValidateModelState]
        [SwaggerOperation("ListeTousLesEncheres")]
        public List<Enchere> ListeTousLesEncheres()
        {
            List<Enchere> lstench = new List<Enchere>();
            TbEnchereDAOImpl enchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> ListEnchere = enchereDAOImpl.ListeAll();
            foreach (var child in ListEnchere)
            {
                var enumDisplayStatus = (StatutEnchere)child.Statut;
                string valeurEnum = enumDisplayStatus.ToString();
                lstench.Add(new Enchere(child.IdEncheres,
                                        child.CommentiareValidation,
                                        child.DateCloture,
                                        child.DateCommandeDeCloture,
                                        child.DateOuverture,
                                        child.MontatDepart,
                                        valeurEnum,
                                       child.Propriete));
            }
            return lstench;
        }

        // POST api/<controller>
        [HttpPost,Route("api/encheres")]
        [SwaggerOperation("EnregistrerEnchere")]
        public void EnregistrerEnchere( Enchere enchere)
        {
        }

        // PUT api/<controller>/5
        [HttpPut,Route("api/encheres/{Id}")]
        [SwaggerOperation("ModifierEnchere")]
        public void ModifierEnchere(int id, Enchere enchere)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete, Route("api/encheres/{Id}")]
        [SwaggerOperation("SupprimerEnchere")]
        public void SupprimerEnchere(int id)
        {
        }
    }
}