﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.mbds.haiti.immoinfo.WebApi.Controllers
{
    public class ProprietesController : ApiController
    {
        // GET api/<controller>
        [HttpGet,Route("listeProprietes")]
        public IEnumerable<string> listerProprietes()
        {
             
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost, Route("ajouterPropriete")]
        public void AjouterPropriete([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut, Route("modifierPropriete/{id}")]
        public void ModifierPropriete(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete, Route("supprimerPropriete/{id}")]
        public void SupprimerPropriete(int id)
        {
        }

       
    }
}