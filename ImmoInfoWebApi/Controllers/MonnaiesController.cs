﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.mbds.haiti.immoinfo.WebApi.Controllers
{
    public class MonnaiesController : ApiController
    {
        // GET api/<controller>
        [HttpGet,Route("listeMonnaies")]
        public IEnumerable<string> ListerMonnaies()
        {
            return new string[] { "HTG", "USD" };
        }
    }
}