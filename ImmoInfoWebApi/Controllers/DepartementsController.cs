﻿using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.mbds.haiti.immoinfo.WebApi.Controllers
{
    public class DepartementsController : ApiController
    {
        // GET api/<controller>
        [HttpGet, Route("api/departements", Name = "List des départements")]
        public List<Departement> ListeDepartement()
        {
            TbDepartementDAOImpl DeptDAOImpl = new TbDepartementDAOImpl();
            
            List<tbDepartement> ListDept = DeptDAOImpl.ListeAll();
            List<Departement> lstDept = new List<Departement>();
            foreach (var child in ListDept)
            {
                lstDept.Add(new Departement(child.IdDept, child.Libelle));
            }
            return lstDept;
        }

    }
}