﻿using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.WebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.mbds.haiti.immoinfo.WebApi.Controllers
{
   // [RoutePrefix("/api/villes")]
    public class VillesController : ApiController
    {
        // GET api/<controller>/5
        [System.Web.Http.HttpGet, System.Web.Http.Route("api/villes/{dept_Id}",Name="Liste des villes par Département")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public List<Ville> ListerVilles(int dept_Id)
        {
            TbVilleDAOImpl villeDAOImpl = new TbVilleDAOImpl();
            List<Ville> listeVilles = new List<Ville>();

            List<tbVille> ListUtil = villeDAOImpl.ListeVilleParDepartement(dept_Id);
            foreach (var child in ListUtil)
            {
                listeVilles.Add(new Ville(child.IdVille, child.Libelle, child.Departement));
            }
            return  listeVilles;
        }

        
    }
}