﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public class Propriete
    {

        public int IdProp { get; set; }
        public string Adresse { get; set; }
        public int Ville { get; set; }
        public string DescriptionSite { get; set; }
        public int Statut { get; set; }
        public decimal MontantSouhaite { get; set; }
        public int Monnaie { get; set; }
        public decimal Perimetre { get; set; }
        public decimal Superficie { get; set; }
        public int RefProp { get; set; }
    }
}