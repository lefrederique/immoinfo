﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public enum StatutCompte
    {
        Actif = 1,
        Annulé = 2,
        En_Approbation = 3
    }
}