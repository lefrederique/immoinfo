﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    [DataContract]
    public class Departement
    {
        public Departement(int IdDept,string Libelle)
        {
            this.IdDept = IdDept;
            this.Libelle = Libelle;
          //  this.Villes = Villes;
        }
        [DataMember]
        public int IdDept { get; set; }
        [DataMember]
        public string Libelle { get; set; }
       // [DataMember]
        //public virtual ICollection<Ville> Villes { get; set; }
    }
}