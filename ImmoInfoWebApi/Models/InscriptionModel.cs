﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public class InscriptionModel
    {

        public Proprietaire proprietaire { get; set; }

        public List<Propriete> proprietes { get; set; }


        public InscriptionModel()
        {
            proprietes = new List<Propriete>();
        }

    }
}