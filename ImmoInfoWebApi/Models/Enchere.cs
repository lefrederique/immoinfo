﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    [DataContract]
    public class Enchere
    {
        #region Constructeur
            public Enchere( int IdEncheres, 
                            string CommentiareValidation,       
                            DateTime DateCloture, 
                            DateTime DateCommandeDeCloture,  
                            DateTime DateOuverture, 
                            decimal MontatDepart,  
                            string Statut, 
                            int Propriete)
            {
            
                this.IdEncheres = IdEncheres;
                this.CommentiareValidation = CommentiareValidation;
                this.DateCloture = DateCloture;
                this.DateCommandeDeCloture = DateCommandeDeCloture;
                this.DateOuverture = DateOuverture;
                this.MontatDepart = MontatDepart;
                this.Statut = Statut;
                this.Propriete = Propriete;
            }
        #endregion

        # region Proprietes
            [DataMember]
            public int IdEncheres { get; set; }

            [DataMember]
            public string CommentiareValidation { get; set; }
        
            [DataMember]
            public DateTime DateCloture { get; set; }

            [DataMember]
            public DateTime DateCommandeDeCloture { get; set; }

            [DataMember]
            public DateTime DateOuverture { get; set; }

            [DataMember]
            public decimal MontatDepart { get; set; }

            [DataMember]
            public string Statut { get; set; }

            [DataMember]
            public int Propriete { get; set; }
        //public virtual ICollection<tbOffre> tbOffres { get; set; }
       // [DataMember]
       // public virtual tbStatutEnchere tbStatutEnchere { get; set; }
            // public virtual tbPropriete tbPropriete { get; set; }
        #endregion
    }
}