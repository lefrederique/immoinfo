﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{ 
    
    public class Monnaie
    {
        public int IdMonnaie { get; set; }
        public string Abreviation { get; set; }
        public string Description { get; set; }
    }
}