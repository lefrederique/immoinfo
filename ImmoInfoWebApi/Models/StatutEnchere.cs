﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public enum StatutEnchere
    {
        Ouvert =1 ,
	    Validé =2,
	    Annulé =3,
	    Cloturé =4,
    }
}