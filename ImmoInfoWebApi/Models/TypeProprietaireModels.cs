﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public class TypeProprietaireModels
    {
        public int IdTypeProp { get; set; }
        public string Description { get; set; }
    }
}