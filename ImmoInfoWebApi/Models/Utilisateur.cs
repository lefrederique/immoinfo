﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public class Utilisateur
    {
        public int IdUtil { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        // public int Statut { get; set; }
        public Privilege Privilege { get; set; }
        public StatutCompte Statut { get; set; }

    }
}