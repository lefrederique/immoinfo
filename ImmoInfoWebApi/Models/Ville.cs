﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    [DataContract]
    public class Ville
    {
        #region Constructeur
            public Ville(int IdVille,String Libelle, int Departement) 
            {
                this.IdVille = IdVille;
                this.Libelle = Libelle;
                this.Departement = Departement;
            }
        #endregion

        #region Propriétés
            [DataMember]
            public int IdVille { get; set; }

            [DataMember(Name ="Ville")]
            public string Libelle { get; set; }

           // [DataMember]
            public int Departement { get; set; }
        #endregion
    }
}