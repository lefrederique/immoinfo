﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public class Proprietaire
    {
        public int IdProp { get; set; }

        public string Adresse { get; set; }

        public string Identification { get; set; }

        public string Nom { get; set; }

        public int TypeProprietaire { get; set; }

        public string Email { get; set; }

        public int Utilisateur { get; set; }
    }
}