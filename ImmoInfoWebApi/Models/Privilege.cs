﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.mbds.haiti.immoinfo.WebApi.Models
{
    public enum Privilege
    {
        Administrateur,
        Propriétaire
    }
}