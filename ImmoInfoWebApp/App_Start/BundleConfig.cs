﻿using System.Web;
using System.Web.Optimization;

namespace ImmoInfoWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                   "~/Scripts/plugins.bundle.js",
                   "~/Scripts/scripts.bundle.js"));


            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                    "~/Scripts/login.js"));

            bundles.Add(new ScriptBundle("~/bundles/loginp").Include(
                "~/Scripts/loginp.js"));
            bundles.Add(new ScriptBundle("~/bundles/wizard").Include(
                "~/Scripts/wizard.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatablestb").Include(
              "~/Scripts/datatablestb.js"));
            bundles.Add(new ScriptBundle("~/bundles/columnrendering").Include(
               "~/Scripts/columnrendering.js"));
           


            bundles.Add(new StyleBundle("~/login/css").Include(
                      "~/Content/login.css"));

            bundles.Add(new StyleBundle("~/loginp/css").Include(
                   "~/Content/loginp.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/plugins.bundle.css",
                      "~/Content/style.bundle.css"));

            bundles.Add(new StyleBundle("~/wizard/css").Include(
                   "~/Content/wizard.css"));

            bundles.Add(new StyleBundle("~/datatablestb/css").Include(
                "~/Content/datatablestb.css"));


        }
    }
}
