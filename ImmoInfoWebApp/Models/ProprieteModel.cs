﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class ProprieteModel
    {

        public int IdProp { get; set; }
        public string Adresse { get; set; }
        public int Ville { get; set; }
        public string VilleLibelle { get; set; }
        public string DescriptionSite { get; set; }
        public int Statut { get; set; }
        public string StatutLibelle { get; set; }
        public decimal MontantSouhaite { get; set; }
        public int Monnaie { get; set; }
        public string MonnaieLibelle { get; set; }
        public decimal Perimetre { get; set; }
        public decimal Superficie { get; set; }
        public int RefProp { get; set; }
        public string RefPropNom { get; set; }
    }
}