﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class StatutCompte
    {
        public int IdStatutCpte { get; set; }
        public string Description { get; set; }
    }
}