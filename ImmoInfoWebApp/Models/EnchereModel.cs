﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class EnchereModel
    {

        public int IdEncheres { get; set; }
        public string CommentiareValidation { get; set; }
        public System.DateTime DateCloture { get; set; }
        public System.DateTime DateCommandeDeCloture { get; set; }
        public System.DateTime DateOuverture { get; set; }
        public decimal MontatDepart { get; set; }
        public int Statut { get; set; }
        public String StatutLibelle { get; set; }



        public int Propriete { get; set; }
    }
}