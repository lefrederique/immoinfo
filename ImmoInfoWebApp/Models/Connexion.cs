﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class Connexion
    {
        [DisplayName("Email")]
        [Required]
        public String Login { get; set; }

        [DisplayName("Mot de passe")]
        [Required]
        public String Password { get; set; }
    }
}