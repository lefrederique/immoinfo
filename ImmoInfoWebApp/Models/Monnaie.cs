﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class Monnaie
    {
        public int IdMonnaie { get; set; }
        public string Abreviation { get; set; }
        public string Description { get; set; }
    }
}