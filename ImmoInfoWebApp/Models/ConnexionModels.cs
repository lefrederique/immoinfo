﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class ConnexionModels
    {
        [DisplayName("Email")]
        [Required(ErrorMessage = "Il faut entrer un email ")]
        public String Login { get; set; }

        [DisplayName("Mot de passe")]
        [Required(ErrorMessage ="Il faut entrer un mot de passe ")]
        public String Password { get; set; }
    }
}