﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class Ville
    {
        public int IdVille { get; set; }
        public string Libelle { get; set; }
        public int Departement { get; set; }
    }
}