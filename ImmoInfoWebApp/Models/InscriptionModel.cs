﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class InscriptionModel
    {

        public ProprietaireModel proprietaire { get; set; }

        public List<ProprieteModel> proprietes { get; set; }


        public InscriptionModel()
        {
            proprietes = new List<ProprieteModel>();
        }

    }
}