﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class ReponseModels <T>
    {
        public bool Success { get; set; }

        public int Code { get; set; }

        public string Message { get; set; }

        public T Retour { get; set; }

    }
}