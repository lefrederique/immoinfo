﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class PrivilegeModels
    {
        public string IdPriv { get; set; }
        public string Description { get; set; }
    }
}