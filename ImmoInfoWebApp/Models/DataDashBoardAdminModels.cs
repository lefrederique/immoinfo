﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class DataDashBoardAdminModels
    {
        public string Email {get; set;}
        public string Identification {get; set;}
        public string Statut {get; set;}
        public string Privilege {get; set;}
        public int NbrProprietaire {get; set;}
        public int NbrPropriete {get; set;}
        public int NbrEnchere {get; set;}
        public int NbrEnchereEncours {get; set;}

        public List<EnchereModel> ListEnchereEnCours { get; set; }

        public List<OffreModels> ListOffreModels { get; set; }



    }
}