﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImmoInfoWebApp.Models
{
    public class OffreModels
    {

        public int IdOffre { get; set; }
        public System.DateTime DateFormulation { get; set; }
        public decimal Montant { get; set; }
        public int Proprietaire { get; set; }
        public string ProprietaireNom { get; set; }
        public int Enchere { get; set; }
    }
}