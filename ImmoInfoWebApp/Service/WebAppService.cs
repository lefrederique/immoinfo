﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImmoInfoWebApp.Models;
using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.Entities.IMException;
using System.Collections.ObjectModel;
using System.Net.Mail;

namespace ImmoInfoWebApp.Service
{
    public class WebAppService
    {

        public List<TypeProprietaireModels> findAllTypeProprietaire()
        {
            List<TypeProprietaireModels> proprietaireModels = new List<TypeProprietaireModels>();
            TbTypeProprietaireDAOImpl tbTypeProprietaireDAOImpl = new TbTypeProprietaireDAOImpl();
            List<tbTypeProprietaire> tbTypeProprietaires = tbTypeProprietaireDAOImpl.ListeAll();

            foreach (tbTypeProprietaire tbTP in tbTypeProprietaires)
            {
                TypeProprietaireModels tempP = new TypeProprietaireModels();
                tempP.IdTypeProp = tbTP.IdTypeProp;
                tempP.Description = tbTP.Description;
                proprietaireModels.Add(tempP);
            }
            return proprietaireModels;
        }


        public List<Ville> findAllVille() {

            List<Ville> villes = new List<Ville>();
            TbVilleDAOImpl tbVilleDAOImpl = new TbVilleDAOImpl();
            List<tbVille> tbVilles = tbVilleDAOImpl.ListeAll();

            foreach (tbVille tbTP in tbVilles)
            {
                Ville tempP = new Ville();
                tempP.IdVille = tbTP.IdVille;
                tempP.Libelle = tbTP.Libelle;
                villes.Add(tempP);
            }

            return villes;
        }


        public List<Monnaie> findAllMonnaie() {
            List<Monnaie> monnaies = new List<Monnaie>();
            TbMonnaieDAOImpl tbMonnaieDAOImpl = new TbMonnaieDAOImpl();
            List<tbMonnaie> tbMonnaies = tbMonnaieDAOImpl.ListeAll();

            foreach (tbMonnaie tbTP in tbMonnaies)
            {
                Monnaie tempP = new Monnaie();
                tempP.IdMonnaie = tbTP.IdMonnaie;
                tempP.Abreviation = tbTP.Abreviation;
                tempP.Description = tbTP.Description;
                monnaies.Add(tempP);
            }

            return monnaies;

        }

        public ReponseModels<InscriptionModel> EnregistrementProprietaire(InscriptionModel inscriptionModel) {

            ReponseModels<InscriptionModel> reponseModels = new ReponseModels<InscriptionModel>();

            TbProprietaireDAOImpl tbProprietaireDAOImpl = new TbProprietaireDAOImpl();
            tbProprietaire tbProp = new tbProprietaire();
            // Affectation Utilisateur
            tbProp.tbUtilisateur = new tbUtilisateur();
            tbProp.tbUtilisateur.Email = inscriptionModel.proprietaire.Email;
            tbProp.tbUtilisateur.Privilege = "USER";
            Random rd = new Random();
            int rand_num = rd.Next(10000, 100000);
            tbProp.tbUtilisateur.Password = rand_num.ToString();
            tbProp.tbUtilisateur.Statut = 3;

            // Affectation proprietaire
            tbProp.Nom = inscriptionModel.proprietaire.Nom;
            tbProp.Adresse = inscriptionModel.proprietaire.Adresse;
            tbProp.Identification = inscriptionModel.proprietaire.Identification;
            tbProp.TypeProprietaire = inscriptionModel.proprietaire.TypeProprietaire;

            // Affection
            tbProp.tbProprietes = new Collection<tbPropriete>(); 
            foreach (ProprieteModel tb in inscriptionModel.proprietes)
            {
                tbPropriete tbTemp = new tbPropriete();
                tbTemp.Adresse = tb.Adresse;
                tbTemp.Ville = tb.Ville;
                tbTemp.DescriptionSite = tb.DescriptionSite;
                tbTemp.Statut = tb.Statut;
                tbTemp.MontantSouhaite = tb.MontantSouhaite;
                tbTemp.Monnaie = tb.Monnaie;
                tbTemp.Perimetre = tb.Perimetre;
                tbTemp.Superficie = tb.Superficie;
                tbTemp.Statut = 1;

                tbProp.tbProprietes.Add(tbTemp);
            }

            try
            {
                tbProprietaireDAOImpl.Inscription(tbProp);
                reponseModels.Success = true;
                reponseModels.Code = 200;
                reponseModels.Message = "Operation reussie";
            }
            catch (ImmoAppException exc)
            {
                if (exc.Code == 200)
                {
                    reponseModels.Success = false;
                    reponseModels.Code = 400;
                    reponseModels.Message = exc.Message;
                }
                else
                {
                    reponseModels.Success = false;
                    reponseModels.Code = 400;
                    reponseModels.Message = "Erreur non capture par le systeme (1)";
                }
                System.Diagnostics.Debug.WriteLine("--->> in web app 3" + exc.Message);
            }
            catch (Exception exc2)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "Erreur non capture par le systeme (2)";
                System.Diagnostics.Debug.WriteLine("--->> in web app 3" + exc2.Message);
              //  System.Diagnostics.Debug.WriteLine("--->> in web app 3" + exc2.InnerException.Message);
                System.Diagnostics.Debug.WriteLine("--->> 2" + exc2.ToString());
            }


            //SendEmail("");


            return reponseModels;
        }


        public void SendEmail(Email email) {
            MailMessage mm = new MailMessage();

            foreach (string to in email.To)
            {
                mm.To.Add(to);
            }

            foreach (string cc in email.Cc)
            {
                mm.CC.Add(cc);
            }
            mm.Subject = email.Subject;
            mm.Body = email.Body;
            mm.From = new MailAddress("mbdsimmoinfo@gmail.com");
            mm.IsBodyHtml = email.Html;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = true;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("mbdsimmoinfo@gmail.com", "Immopass1234");
            smtp.Send(mm);
        }


        public ReponseModels<UtilisateurModels> Connexion(ConnexionModels connexion) {

            ReponseModels<UtilisateurModels> reponseModels = new ReponseModels<UtilisateurModels>();
            if (connexion ==null) {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "Veuillez verifier les parametres de connexion";
                return reponseModels;
            }

      
            UtilisateurModels utilisateurModels = new UtilisateurModels();

            TbUtilisateurDAOImpl tbUtilisateurDAOImpl = new TbUtilisateurDAOImpl();
            tbUtilisateur tbUt = tbUtilisateurDAOImpl.Connexion(connexion.Login, connexion.Password);

            if(tbUt == null)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "L'utilisateur n'existe pas";
            }
            else
            {
                utilisateurModels.IdUtil = tbUt.IdUtil;
                utilisateurModels.Password = tbUt.Password;
                utilisateurModels.Privilege = tbUt.Privilege;
                utilisateurModels.Statut = tbUt.Statut;
                utilisateurModels.Email = tbUt.Email;

                switch (utilisateurModels.Statut)
                {

                    case 1:
                        {
                            reponseModels.Success = true;
                            reponseModels.Code = 200;
                            reponseModels.Message = "Opération reussie";
                            break;
                        }

                    case 2:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est désactivé sur le systeme";
                            break;
                        }

                    case 3:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est en attente d'apporbation";
                            break;
                        }

                    default :
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 500;
                            reponseModels.Message = "Veuillez contacter l'administrateur systeme (Code : 500)";
                            break;
                        }
                }

                // Affectation de l'objet 
                reponseModels.Retour = utilisateurModels;

            }

            return reponseModels;
        }


        public ReponseModels<UtilisateurModels> findUtilisateurByUsername(string username)
        {

            ReponseModels<UtilisateurModels> reponseModels = new ReponseModels<UtilisateurModels>();
            if (username == null)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "Veuillez verifier les parametres de connexion";
                return reponseModels;
            }


            UtilisateurModels utilisateurModels = new UtilisateurModels();

            TbUtilisateurDAOImpl tbUtilisateurDAOImpl = new TbUtilisateurDAOImpl();
            tbUtilisateur tbUt = tbUtilisateurDAOImpl.findByUsername(username);

            if (tbUt == null)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "L'utilisateur n'existe pas";
            }
            else
            {
                utilisateurModels.IdUtil = tbUt.IdUtil;
                utilisateurModels.Password = tbUt.Password;
                utilisateurModels.Privilege = tbUt.Privilege;
                utilisateurModels.Statut = tbUt.Statut;
                utilisateurModels.Email = tbUt.Email;

                switch (utilisateurModels.Statut)
                {

                    case 1:
                        {
                            reponseModels.Success = true;
                            reponseModels.Code = 200;
                            reponseModels.Message = "Opération reussie";
                            break;
                        }

                    case 2:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est désactivé sur le systeme";
                            break;
                        }

                    case 3:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est en attente d'apporbation";
                            break;
                        }

                    default:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 500;
                            reponseModels.Message = "Veuillez contacter l'administrateur systeme (Code : 500)";
                            break;
                        }
                }

                // Affectation de l'objet 
                reponseModels.Retour = utilisateurModels;

            }

            return reponseModels;
        }


        public ReponseModels<UtilisateurModels> findUtilisateurByUsernameAndRole(string username, string role)
        {

            ReponseModels<UtilisateurModels> reponseModels = new ReponseModels<UtilisateurModels>();
            if (username == null)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "Veuillez verifier les parametres de connexion";
                return reponseModels;
            }


            UtilisateurModels utilisateurModels = new UtilisateurModels();

            TbUtilisateurDAOImpl tbUtilisateurDAOImpl = new TbUtilisateurDAOImpl();
            tbUtilisateur tbUt = tbUtilisateurDAOImpl.findByUsernameAndRole(username, role);

            if (tbUt == null)
            {
                reponseModels.Success = false;
                reponseModels.Code = 400;
                reponseModels.Message = "L'utilisateur n'existe pas";
            }
            else
            {
                utilisateurModels.IdUtil = tbUt.IdUtil;
                utilisateurModels.Password = tbUt.Password;
                utilisateurModels.Privilege = tbUt.Privilege;
                utilisateurModels.Statut = tbUt.Statut;
                utilisateurModels.Email = tbUt.Email;

                switch (utilisateurModels.Statut)
                {

                    case 1:
                        {
                            reponseModels.Success = true;
                            reponseModels.Code = 200;
                            reponseModels.Message = "Opération reussie";
                            break;
                        }

                    case 2:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est désactivé sur le systeme";
                            break;
                        }

                    case 3:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 300;
                            reponseModels.Message = "Votre compte est en attente d'apporbation";
                            break;
                        }

                    default:
                        {
                            reponseModels.Success = false;
                            reponseModels.Code = 500;
                            reponseModels.Message = "Veuillez contacter l'administrateur systeme (Code : 500)";
                            break;
                        }
                }

                // Affectation de l'objet 
                reponseModels.Retour = utilisateurModels;

            }

            return reponseModels;
        }


        public List<PrivilegeModels> findAllPrivilege()
        {
            List<PrivilegeModels> privilegeModels = new List<PrivilegeModels>();
            TbPrivilegeDAOImpl tbPrivilegeDAOImpl  = new TbPrivilegeDAOImpl();
            List<tbPrivilege> tbPrivileges = tbPrivilegeDAOImpl.ListeAll();

            foreach (tbPrivilege tbTP in tbPrivileges)
            {
                PrivilegeModels tempP = new PrivilegeModels();
                tempP.IdPriv = tbTP.IdPriv;
                tempP.Description = tbTP.Description;
                privilegeModels.Add(tempP);
            }
            return privilegeModels;
        }


        public List<ProprietaireModel> findAllProprietaire()
        {
            List<ProprietaireModel> proprietaireModel = new List<ProprietaireModel>();
            TbProprietaireDAOImpl tbProprietaireDAOImpl = new TbProprietaireDAOImpl();
            List<tbProprietaire> tbProprietaires = tbProprietaireDAOImpl.ListeAll();

            foreach (tbProprietaire tbTP in tbProprietaires)
            {
                ProprietaireModel tempP = new ProprietaireModel();
                tempP.IdProp = tbTP.IdProp;
                tempP.Adresse = tbTP.Adresse;
                tempP.Identification = tbTP.Identification;
                tempP.Nom = tbTP.Nom;
                tempP.TypeProprietaire = tbTP.TypeProprietaire;
                tempP.LibelleTypeProprietaire = tbTP.tbTypeProprietaire.Description;
                tempP.Email = tbTP.tbUtilisateur.Email;
                tempP.libelleStatut = tbTP.tbUtilisateur.tbStatutCompte.Description;
                tempP.idStatut = tbTP.tbUtilisateur.tbStatutCompte.IdStatutCpte;
                tempP.Utilisateur = tbTP.tbUtilisateur.IdUtil;
                proprietaireModel.Add(tempP);
            }
            return proprietaireModel;
        }

        public List<StatutCompte> findAllStatutCompte()
        {
            List<StatutCompte> StatutComptes = new List<StatutCompte>();
            TbStatutCompteDAOImpl tbStatutCompteDAOImpl = new TbStatutCompteDAOImpl();
            List<tbStatutCompte> tbStatutComptes = tbStatutCompteDAOImpl.ListeAll();

            foreach (tbStatutCompte tbTP in tbStatutComptes)
            {
                StatutCompte tempP = new StatutCompte();
                tempP.IdStatutCpte = tbTP.IdStatutCpte;
                tempP.Description = tbTP.Description;
                StatutComptes.Add(tempP);
            }
            return StatutComptes;
        }

        public void ModifierStatutUtilisateur(ChangementStatutModels changementStatutModels)
        {
            TbUtilisateurDAOImpl tbUtilisateurDAOImpl = new TbUtilisateurDAOImpl();
            tbUtilisateurDAOImpl.ModifierStatutUtilisateur(changementStatutModels.IdUtil, changementStatutModels.IdStatut);
        }


        public List<ProprieteModel> findAllPropriete()
        {
            List<ProprieteModel> proprieteModels = new List<ProprieteModel>();
            TbProprieteDAOImpl tbProprieteDAOImpl = new TbProprieteDAOImpl();
            List<tbPropriete> tbProprietes = tbProprieteDAOImpl.ListeAll();

            foreach (tbPropriete tbTP in tbProprietes)
            {
                ProprieteModel tempP = new ProprieteModel();


                tempP.IdProp = tbTP.IdProp;
                tempP.Adresse = tbTP.Adresse;
                tempP.Ville = tbTP.Ville;
                tempP.VilleLibelle = tbTP.tbVille.Libelle;
                tempP.DescriptionSite = tbTP.DescriptionSite;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutPropriete.Description;
                tempP.MontantSouhaite = tbTP.MontantSouhaite;
                tempP.Monnaie = tbTP.Monnaie;
                tempP.MonnaieLibelle = tbTP.tbMonnaie.Abreviation;
                tempP.Perimetre = tbTP.Perimetre;
                tempP.Superficie = tbTP.Superficie;
                tempP.RefProp = tbTP.Proprietaire;
                tempP.RefPropNom = tbTP.tbProprietaire.Nom;


                proprieteModels.Add(tempP);
            }
            return proprieteModels;

        }






        public ProprieteModel findOnePropriete(int id)
        {
       
            TbProprieteDAOImpl tbProprieteDAOImpl = new TbProprieteDAOImpl();
            tbPropriete tbTP = tbProprieteDAOImpl.FindOne(id);
            ProprieteModel tempP = new ProprieteModel();
            tempP.IdProp = tbTP.IdProp;
            tempP.Adresse = tbTP.Adresse;
            tempP.Ville = tbTP.Ville;
            tempP.VilleLibelle = tbTP.tbVille.Libelle;
            tempP.DescriptionSite = tbTP.DescriptionSite;
            tempP.Statut = tbTP.Statut;
            tempP.StatutLibelle = tbTP.tbStatutPropriete.Description;
            tempP.MontantSouhaite = tbTP.MontantSouhaite;
            tempP.Monnaie = tbTP.Monnaie;
            tempP.MonnaieLibelle = tbTP.tbMonnaie.Abreviation;
            tempP.Perimetre = tbTP.Perimetre;
            tempP.Superficie = tbTP.Superficie;
            tempP.RefProp = tbTP.Proprietaire;
            tempP.RefPropNom = tbTP.tbProprietaire.Nom;
            return tempP;
        }




        public List<ProprieteModel> findAllProprieteParClient(string login)
        {
            List<ProprieteModel> proprieteModels = new List<ProprieteModel>();
            TbProprieteDAOImpl tbProprieteDAOImpl = new TbProprieteDAOImpl();
            List<tbPropriete> tbProprietes = tbProprieteDAOImpl.ListAllParLoginProprietaire(login);

            foreach (tbPropriete tbTP in tbProprietes)
            {
                ProprieteModel tempP = new ProprieteModel();


                tempP.IdProp = tbTP.IdProp;
                tempP.Adresse = tbTP.Adresse;
                tempP.Ville = tbTP.Ville;
                tempP.VilleLibelle = tbTP.tbVille.Libelle;
                tempP.DescriptionSite = tbTP.DescriptionSite;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutPropriete.Description;
                tempP.MontantSouhaite = tbTP.MontantSouhaite;
                tempP.Monnaie = tbTP.Monnaie;
                tempP.MonnaieLibelle = tbTP.tbMonnaie.Abreviation;
                tempP.Perimetre = tbTP.Perimetre;
                tempP.Superficie = tbTP.Superficie;
                tempP.RefProp = tbTP.Proprietaire;
                tempP.RefPropNom = tbTP.tbProprietaire.Nom;


                proprieteModels.Add(tempP);
            }
            return proprieteModels;

        }


        public List<EnchereModel> findAllEchere()
        {
            List<EnchereModel> enchereModels = new List<EnchereModel>();
            TbEnchereDAOImpl tbEnchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> tbEnc = tbEnchereDAOImpl.ListeAll();

            foreach (tbEnchere tbTP in tbEnc)
            {
                EnchereModel tempP = new EnchereModel();


                tempP.IdEncheres = tbTP.IdEncheres;
                tempP.CommentiareValidation = tbTP.CommentiareValidation;
                tempP.DateCloture = tbTP.DateCloture;
                tempP.DateCommandeDeCloture = tbTP.DateCommandeDeCloture;
                tempP.DateOuverture = tbTP.DateOuverture;
                tempP.MontatDepart = tbTP.MontatDepart;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutEnchere.Description;
                tempP.Propriete = tbTP.Propriete;
                enchereModels.Add(tempP);
            }
            return enchereModels;

        }
        

        public List<EnchereModel> findAllEchereParClient(string login)
        {
            List<EnchereModel> enchereModels = new List<EnchereModel>();
            TbEnchereDAOImpl tbEnchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> tbEnc = tbEnchereDAOImpl.ListeEnchereParProprietaire(login);

            foreach (tbEnchere tbTP in tbEnc)
            {
                EnchereModel tempP = new EnchereModel();


                tempP.IdEncheres = tbTP.IdEncheres;
                tempP.CommentiareValidation = tbTP.CommentiareValidation;
                tempP.DateCloture = tbTP.DateCloture;
                tempP.DateCommandeDeCloture = tbTP.DateCommandeDeCloture;
                tempP.DateOuverture = tbTP.DateOuverture;
                tempP.MontatDepart = tbTP.MontatDepart;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutEnchere.Description;
                tempP.Propriete = tbTP.Propriete;
                enchereModels.Add(tempP);
            }
            return enchereModels;

        }


        public List<EnchereModel> findAllEchereAutreParClient(string login)
        {
            List<EnchereModel> enchereModels = new List<EnchereModel>();
            TbEnchereDAOImpl tbEnchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> tbEnc = tbEnchereDAOImpl.ListeAutreEnchereParProprietaire(login);

            foreach (tbEnchere tbTP in tbEnc)
            {
                EnchereModel tempP = new EnchereModel();


                tempP.IdEncheres = tbTP.IdEncheres;
                tempP.CommentiareValidation = tbTP.CommentiareValidation;
                tempP.DateCloture = tbTP.DateCloture;
                tempP.DateCommandeDeCloture = tbTP.DateCommandeDeCloture;
                tempP.DateOuverture = tbTP.DateOuverture;
                tempP.MontatDepart = tbTP.MontatDepart;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutEnchere.Description;
                tempP.Propriete = tbTP.Propriete;
                enchereModels.Add(tempP);
            }
            return enchereModels;

        }

        public DataDashBoardAdminModels DataDashBoardAdmin(string login)
        {
            DataDashBoardAdminModels dataDashBoardAdminModels = new DataDashBoardAdminModels();

            TbUtilisateurDAOImpl tbUtilisateurDAOImpl = new TbUtilisateurDAOImpl();
            tbUtilisateur utilisateur =  tbUtilisateurDAOImpl.findByUsername(login);

            dataDashBoardAdminModels.Email = utilisateur.Email;
            dataDashBoardAdminModels.Statut = utilisateur.tbStatutCompte.Description;
            dataDashBoardAdminModels.Privilege = utilisateur.Privilege;

            // Nbre
            TbProprietaireDAOImpl tbProprietaireDAOImpl = new TbProprietaireDAOImpl();
            List<tbProprietaire> tbProprietairesL = tbProprietaireDAOImpl.ListeAll();
            dataDashBoardAdminModels.NbrProprietaire = (tbProprietairesL == null ? 0 : tbProprietairesL.Count);

            TbProprieteDAOImpl tbProprieteDAOImpl = new TbProprieteDAOImpl();
            List<tbPropriete> tbProprietesL = tbProprieteDAOImpl.ListeAll();
            dataDashBoardAdminModels.NbrPropriete = (tbProprietesL == null ? 0 : tbProprietesL.Count);


            TbEnchereDAOImpl tbEnchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> tbEncheresL = tbEnchereDAOImpl.ListeAll();
            dataDashBoardAdminModels.NbrEnchere  = (tbEncheresL == null ? 0 : tbEncheresL.Count);

            List<tbEnchere> tbEncheresLEncours = tbEnchereDAOImpl.ListerEnchereParStatut(1);
            dataDashBoardAdminModels.NbrEnchereEncours = (tbEncheresLEncours == null ? 0 : tbEncheresLEncours.Count);

            List<EnchereModel> enchereModelsL = new List<EnchereModel>();

            foreach (tbEnchere tbTP in tbEncheresLEncours)
            {
                EnchereModel tempP = new EnchereModel();
                tempP.IdEncheres = tbTP.IdEncheres;
                tempP.CommentiareValidation = tbTP.CommentiareValidation;
                tempP.DateCloture = tbTP.DateCloture;
                tempP.DateCommandeDeCloture = tbTP.DateCommandeDeCloture;
                tempP.DateOuverture = tbTP.DateOuverture;
                tempP.MontatDepart = tbTP.MontatDepart;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutEnchere.Description;
                tempP.Propriete = tbTP.Propriete;
                enchereModelsL.Add(tempP);
            }
            dataDashBoardAdminModels.ListEnchereEnCours = enchereModelsL;


            // Listing 
            List<OffreModels> offreModels = new List<OffreModels>();
            TbOffreDAOImpl tbOffreDAOImpl = new TbOffreDAOImpl();
            List<tbOffre> tbOffresL = tbOffreDAOImpl.ListeAll();

            foreach (tbOffre tbTP in tbOffresL)
            {
                OffreModels tempP = new OffreModels();


                tempP.IdOffre = tbTP.IdOffre;
                tempP.Montant = tbTP.Montant;
                tempP.DateFormulation = tbTP.DateFormulation;
                tempP.Enchere = tbTP.Enchere;
                tempP.Proprietaire = tbTP.Proprietaire;
                tempP.ProprietaireNom = tbTP.tbProprietaire.Nom;
                offreModels.Add(tempP);
            }

            dataDashBoardAdminModels.ListOffreModels = offreModels;
      

            return dataDashBoardAdminModels;

        }



        public DataDashBoardClientModels DataDashBoardClient(string login)
        {
            DataDashBoardClientModels dataDashBoardClientModels = new DataDashBoardClientModels();

            TbProprietaireDAOImpl tbProprietaireDAOImpl = new TbProprietaireDAOImpl();
            tbProprietaire tbProprit = tbProprietaireDAOImpl.findProprietaireByLogin(login);

            dataDashBoardClientModels.Email = tbProprit.tbUtilisateur.Email;
            dataDashBoardClientModels.Privilege = tbProprit.tbUtilisateur.Privilege;
            dataDashBoardClientModels.Nom = tbProprit.Nom;
            dataDashBoardClientModels.Adresse = tbProprit.Adresse;

            TbProprieteDAOImpl tbProprieteDAOImpl = new TbProprieteDAOImpl();
            List<tbPropriete> tbProprietesL = tbProprieteDAOImpl.ListAllParProprietaire(tbProprit.IdProp);
            dataDashBoardClientModels.NbrPropriete = (tbProprietesL == null ? 0 : tbProprietesL.Count);


            TbEnchereDAOImpl tbEnchereDAOImpl = new TbEnchereDAOImpl();
            List<tbEnchere> tbEncheresL = tbEnchereDAOImpl.ListeEnchereParProprietaire(login);
            dataDashBoardClientModels.NbrEnchere = (tbEncheresL == null ? 0 : tbEncheresL.Count);

            List<tbEnchere> tbEncheresLEncours = tbEnchereDAOImpl.ListeEnchereParProprietaire(login,1);
            dataDashBoardClientModels.NbrEnchereEncours = (tbEncheresLEncours == null ? 0 : tbEncheresLEncours.Count);

            List<EnchereModel> enchereModelsL = new List<EnchereModel>();

            foreach (tbEnchere tbTP in tbEncheresLEncours)
            {
                EnchereModel tempP = new EnchereModel();
                tempP.IdEncheres = tbTP.IdEncheres;
                tempP.CommentiareValidation = tbTP.CommentiareValidation;
                tempP.DateCloture = tbTP.DateCloture;
                tempP.DateCommandeDeCloture = tbTP.DateCommandeDeCloture;
                tempP.DateOuverture = tbTP.DateOuverture;
                tempP.MontatDepart = tbTP.MontatDepart;
                tempP.Statut = tbTP.Statut;
                tempP.StatutLibelle = tbTP.tbStatutEnchere.Description;
                tempP.Propriete = tbTP.Propriete;
                enchereModelsL.Add(tempP);
            }
            dataDashBoardClientModels.ListEnchereEnCours = enchereModelsL;


            // Listing 
            List<OffreModels> offreModels = new List<OffreModels>();
            TbOffreDAOImpl tbOffreDAOImpl = new TbOffreDAOImpl();
            List<tbOffre> tbOffresL = tbOffreDAOImpl.ListeAll();

            foreach (tbOffre tbTP in tbOffresL)
            {
                OffreModels tempP = new OffreModels();


                tempP.IdOffre = tbTP.IdOffre;
                tempP.Montant = tbTP.Montant;
                tempP.DateFormulation = tbTP.DateFormulation;
                tempP.Enchere = tbTP.Enchere;
                tempP.Proprietaire = tbTP.Proprietaire;
                tempP.ProprietaireNom = tbTP.tbProprietaire.Nom;
                offreModels.Add(tempP);
            }

            dataDashBoardClientModels.ListOffreModels = offreModels;


            return dataDashBoardClientModels;

        }

    }
}