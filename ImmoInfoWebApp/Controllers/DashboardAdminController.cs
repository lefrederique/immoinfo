﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;
namespace ImmoInfoWebApp.Controllers
{
    public class DashboardAdminController : Controller
    {

        WebAppService appService = new WebAppService();
        // GET: DashboardAdmin
        [Authorize(Roles ="ADMIN")]
        public ActionResult Index()
        {
            DataDashBoardAdminModels dataDashBoardAdmin = new DataDashBoardAdminModels();
            dataDashBoardAdmin = appService.DataDashBoardAdmin(User.Identity.Name);

            ViewBag.dataDashBoardAdmin = dataDashBoardAdmin;
            return View();
        }
    }
}