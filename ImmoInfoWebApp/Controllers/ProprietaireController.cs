﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;

namespace ImmoInfoWebApp.Controllers
{
    public class ProprietaireController : Controller
    {

        WebAppService appService = new WebAppService();
        // GET: Proprietaire
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult ListeProprietaire()
        {


            //var output;

            //int stateNo = Random(0, 7);
            //string states = [
            //    "success",
            //    "light",
            //    "danger",
            //    "success",
            //    "warning",
            //    "dark",
            //    "primary",
            //    "info" ];

            //var state = states[stateNo];
            ViewBag.statutCmpt = appService.findAllStatutCompte();
            List<ProprietaireModel> proprietaireModels = appService.findAllProprietaire();
            ViewBag.listeProprietaire = proprietaireModels;
            return View();
        }


        public ActionResult ChangementStatut(ChangementStatutModels changementStatutModels) {

            appService.ModifierStatutUtilisateur(changementStatutModels);
            return RedirectToAction("ListeProprietaire", "Proprietaire");
        }

    }
}