﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;

namespace ImmoInfoWebApp.Controllers
{
    public class EnchereController : Controller
    {
        WebAppService appService = new WebAppService();
        // GET: Enchere
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListeEnchere()
        {
            List<EnchereModel> enchereModels = appService.findAllEchere();
            ViewBag.listeEnchere = enchereModels;
            return View();
        }

        public ActionResult ListeEnchereClient()
        {
            List<EnchereModel> enchereModels = appService.findAllEchereParClient(User.Identity.Name);
            ViewBag.listeEnchere = enchereModels;
            return View("ListeEnchere");
        }

        public ActionResult ListeEnchereAutreClient()
        {
            List<EnchereModel> enchereModels = appService.findAllEchereAutreParClient(User.Identity.Name);
            ViewBag.listeEnchere = enchereModels;
            return View("ListeEnchere");
        }


        public ActionResult DetailEnchere()
        {

            return View();
        }
    }
}