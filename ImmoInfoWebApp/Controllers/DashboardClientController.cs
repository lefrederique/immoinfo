﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;
namespace ImmoInfoWebApp.Controllers
{
    public class DashboardClientController : Controller
    {

        WebAppService appService = new WebAppService();
        // GET: DashboardClient
        [Authorize(Roles ="USER")]
        public ActionResult Index()
        {
            DataDashBoardClientModels dataDashBoardClient = new DataDashBoardClientModels();
            dataDashBoardClient = appService.DataDashBoardClient(User.Identity.Name);

            ViewBag.dataDashBoardClient = dataDashBoardClient;
            return View();

        }
    }
}