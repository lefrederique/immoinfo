﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;
namespace ImmoInfoWebApp.Controllers
{


    public class ProprieteController : Controller
    {


        WebAppService appService = new WebAppService();
        // GET: Propriete
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles ="ADMIN")]
        public ActionResult ListPropriete()
        {

            List<ProprieteModel> proprieteModels = appService.findAllPropriete();
            ViewBag.listePropriete = proprieteModels;
            return View();
        }

        public ActionResult ListProprieteClient()
        {
            List<ProprieteModel> proprieteModels = appService.findAllProprieteParClient(User.Identity.Name);
            ViewBag.listePropriete = proprieteModels;
            return View("ListPropriete");
        }

        public ActionResult DetailProprite() {

            return View();

        }
    }
}