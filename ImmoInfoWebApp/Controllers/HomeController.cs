﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;


namespace ImmoInfoWebApp.Controllers
{
    public class HomeController : Controller
    {
        TbUtilisateurDAOImpl data_u = new TbUtilisateurDAOImpl();


        public ActionResult Index()
        {
           tbUtilisateur tb = data_u.FindOne(1);

            return Content(tb.Email);
            //return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}