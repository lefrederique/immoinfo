﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImmoInfoWebApp.Models;
using com.mbds.haiti.immoinfo.Entities.DAOImpl;
using com.mbds.haiti.immoinfo.Entities.Models;
using ImmoInfoWebApp.Service;
using System.Web.Security;

namespace ImmoInfoWebApp.Controllers
{
    public class UtilisateurController : Controller
    {

        WebAppService appService = new WebAppService();

        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(ConnexionModels connexion)
        {
            ReponseModels<UtilisateurModels> reponseModels = appService.Connexion(connexion);

            if (reponseModels != null && reponseModels.Success == true)
            {
                FormsAuthentication.SetAuthCookie(reponseModels.Retour.Email, false);
                if (reponseModels.Retour.Privilege == "ADMIN")
                {
                    return RedirectToAction("Index", "DashboardAdmin");
                }
                else 
                {
                    return RedirectToAction("Index", "DashboardClient");
                }
            }
            else {
                ModelState.AddModelError("Error",reponseModels.Message);
                return View();
            }

        }

 
        public ActionResult Enregistrement()
        {
            //return Content(inscriptionModel.proprietaire.Nom + " / "+inscriptionModel.proprietes.Count);
            // return Json(inscriptionModel, JsonRequestBehavior.AllowGet);
            //ReponseModels<InscriptionModel> reponseModels = appService.EnregistrementProprietaire(inscriptionModel);
            return View();
        }

        [HttpPost]
        public ActionResult Inscription(InscriptionModel inscriptionModelData)
        {
            ReponseModels<InscriptionModel> reponseModels = appService.EnregistrementProprietaire(inscriptionModelData);

            if (reponseModels.Success) {
                return RedirectToAction("Enregistrement", "Utilisateur");
            }
            else
            {
                ViewBag.ErrorMessage = reponseModels.Message;
                List<TypeProprietaireModels> typeProprietaireModels = appService.findAllTypeProprietaire();
                ViewBag.typePs = typeProprietaireModels;
                InscriptionModel inscriptionModel = new InscriptionModel();
                return View(inscriptionModel);

            }

       

        }

            public ActionResult Inscription()
        {
            List<TypeProprietaireModels> typeProprietaireModels = appService.findAllTypeProprietaire();
            ViewBag.typePs = typeProprietaireModels;
            InscriptionModel inscriptionModel = new InscriptionModel();
            return View(inscriptionModel);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Utilisateur");
        }

        public ActionResult AddChild()
        {
            ViewBag.lMonnaie = appService.findAllMonnaie();
            ViewBag.lVille = appService.findAllVille();
            return PartialView("ChildRow", new ProprieteModel());
        }
    }
}