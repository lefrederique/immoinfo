﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ImmoInfoWebApp.Service;
using ImmoInfoWebApp.Models;

namespace ImmoInfoWebApp.SecurityConfiguration
{
    public class MyRoleManager : RoleProvider
    {
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            WebAppService webAppService = new WebAppService();
            List<String> dRoles = new List<string>();
            List<PrivilegeModels> lPrivilegeModels =  webAppService.findAllPrivilege();
            foreach (PrivilegeModels pv in lPrivilegeModels)
            {
                dRoles.Add(pv.IdPriv);
            }

            return dRoles.ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
          WebAppService webAppService = new WebAppService();
          ReponseModels<UtilisateurModels> reponseModels =   webAppService.findUtilisateurByUsername(username);
          if (reponseModels.Success) {
             string[] result = { reponseModels.Retour.Privilege };
             return result;
            }
            else
            {
                string[] result = { };
                return result;
            }
            
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            WebAppService webAppService = new WebAppService();
            ReponseModels<UtilisateurModels> reponseModels = webAppService.findUtilisateurByUsernameAndRole(username, roleName);
            if (reponseModels.Success)
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}