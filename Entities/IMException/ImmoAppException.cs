﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mbds.haiti.immoinfo.Entities.IMException
{
    public class ImmoAppException :Exception
    {

        public string Message { get; set; }
        public int Code { get; set; }

        public ImmoAppException()
        {
        }


        public ImmoAppException(string data, int dcode) : base(data)
        {
            Message = data;
            Code = dcode;
        }

    }
}
