//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.mbds.haiti.immoinfo.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbStatutCompte
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbStatutCompte()
        {
            this.tbUtilisateurs = new HashSet<tbUtilisateur>();
        }
    
        public int IdStatutCpte { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbUtilisateur> tbUtilisateurs { get; set; }
    }
}
