﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mbds.haiti.immoinfo.Entities.IDAO
{
    interface ITbEnchereDAO<T> : IDAOBASE<T>
    {
        T ValiderEnchere(int idEnchere);
        T AnnulerEnchere(int idEnchere);
        T CloturerEnchere(int idEnchere);
        List<T> ListerEnchereParStatut(int idStatut);



    

    }
}
