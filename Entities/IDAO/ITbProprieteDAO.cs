﻿using com.mbds.haiti.immoinfo.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mbds.haiti.immoinfo.Entities.IDAO
{
    interface ITbProprieteDAO<T> : IDAOBASE<T>
    {
        tbEnchere MettreAuEncheres(decimal montantDepart, int propriete);

        List<T> ListAllParProprietaire(int idProprietaire);
    }


}
