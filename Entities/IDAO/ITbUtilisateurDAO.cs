﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mbds.haiti.immoinfo.Entities.IDAO
{
    interface ITbUtilisateurDAO<T> : IDAOBASE<T>
    {
        T Connexion(string login, string password);
        T findByUsername(string login);

        T findByUsernameAndRole(string login, string role);

        void ModifierStatutUtilisateur(int idUtil, int idStatut);

    }
}
