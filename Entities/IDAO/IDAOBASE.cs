﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mbds.haiti.immoinfo.Entities.IDAO
{
    interface IDAOBASE <T>
    {
        T Save(T data);
         
        T Update(T data);

         List<T> ListeAll();

        T Delete(T data);

        T FindOne(int Id); 
    }
}
