﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.Entities.IMException;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbProprietaireDAOImpl : ITbProprietaireDAO<tbProprietaire>
    {
        public tbProprietaire Delete(tbProprietaire data)
        {
            throw new NotImplementedException();
        }

        public tbProprietaire FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public tbProprietaire Inscription(tbProprietaire data)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                using (DbContextTransaction transaction = mbdsEntity.Database.BeginTransaction()) {
                    try {

                        tbUtilisateur tbU=    mbdsEntity.tbUtilisateurs.Where(x => x.Email == data.tbUtilisateur.Email).FirstOrDefault<tbUtilisateur>();
                        // Verification Email
                        if (tbU==null)
                        {
                            mbdsEntity.tbUtilisateurs.Add(data.tbUtilisateur);
                            mbdsEntity.SaveChanges();
                        }
                        else
                        {
                           // transaction.Rollback();
                            throw new ImmoAppException("Cet Email existe deja sur le system", 200);
                            
                        }

                        // Verification Identification
                        tbProprietaire tbPro =  mbdsEntity.tbProprietaires.Where<tbProprietaire>(x => x.Identification == data.Identification).FirstOrDefault<tbProprietaire>();
                        if (tbPro==null)
                        {
                            data.Utilisateur = data.tbUtilisateur.IdUtil;
                            mbdsEntity.tbProprietaires.Add(data);
                            mbdsEntity.SaveChanges();
                        }
                        else
                        {
                            //transaction.Rollback();
                            throw new ImmoAppException("Cet Identification existe deja sur le system", 200);
                        }

                        transaction.Commit();

                } catch (ImmoAppException  ex)
                {

                        System.Diagnostics.Debug.WriteLine("Error in Code importatnt");
                        transaction.Rollback();
                
                    throw new ImmoAppException(ex.Message, 200);
                   
                }catch (Exception ee)
                {
                    System.Diagnostics.Debug.WriteLine("--->> e1 " + ee.Message);
                    System.Diagnostics.Debug.WriteLine("--->> e2 " + ee.ToString());
                    System.Diagnostics.Debug.WriteLine("--->> e3" + ee.InnerException.Message);
                    throw new ImmoAppException("Error non defini", 400);
                  
                }

            }
              
                return data;
            }
          
        }

        public List<tbProprietaire> ListeAll()
        {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
             return mbdsEntity.tbProprietaires
                    .Include(x=>x.tbTypeProprietaire)
                    .Include(x=>x.tbUtilisateur)
                    .Include(x=>x.tbUtilisateur.tbStatutCompte)
                    .ToList<tbProprietaire>();
            }
          
        }

        public tbProprietaire Save(tbProprietaire data)
        {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {

                mbdsEntity.tbProprietaires.Add(data);
                mbdsEntity.SaveChanges();
                return data;
            }
        }

        public tbProprietaire Update(tbProprietaire data)
        {
            throw new NotImplementedException();
        }


        public tbProprietaire findProprietaireByLogin(string login)
        {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbProprietaires
                       .Include(x => x.tbTypeProprietaire)
                       .Include(x => x.tbUtilisateur)
                       .Include(x => x.tbUtilisateur.tbStatutCompte)
                       .Where<tbProprietaire>(x=>x.tbUtilisateur.Email == login).FirstOrDefault<tbProprietaire>();
            }

        }

    }
}
