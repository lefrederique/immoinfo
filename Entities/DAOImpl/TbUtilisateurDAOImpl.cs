﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;


namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbUtilisateurDAOImpl : ITbUtilisateurDAO<tbUtilisateur>
    {



        public tbUtilisateur Connexion(string login, string password)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {

                return mbdsEntity.tbUtilisateurs.Where(x => x.Email == login && x.Password == password).FirstOrDefault<tbUtilisateur>();
            }
               
        }

        public tbUtilisateur findByUsername(string login) {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbUtilisateurs
                    .Include(x=>x.tbStatutCompte)
                    .Where(x => x.Email == login).FirstOrDefault<tbUtilisateur>();
            }
        }


        public tbUtilisateur findByUsernameWithPro(string login)
        {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbUtilisateurs
                    .Include(x => x.tbStatutCompte)
                    .Include(x => x.tbProprietaires)
                    .Where(x => x.Email == login).FirstOrDefault<tbUtilisateur>();
            }
        }

        public tbUtilisateur Delete(tbUtilisateur data)
        {
            throw new NotImplementedException();
        }

        public tbUtilisateur FindOne(int Id)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbUtilisateurs.Where(x => x.IdUtil == Id).First<tbUtilisateur>();
            }
              
        }

        public List<tbUtilisateur> ListeAll() 
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbUtilisateurs.ToList<tbUtilisateur>();
            }
               

        }

        public tbUtilisateur Save(tbUtilisateur data)
        {
            throw new NotImplementedException();
        }

        public tbUtilisateur Update(tbUtilisateur data)
        {
            throw new NotImplementedException();
        }

        public tbUtilisateur findByUsernameAndRole(string login, string role)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbUtilisateurs.Where(x => x.Email == login && x.Privilege == role).First<tbUtilisateur>();
            }
        }

        public void ModifierStatutUtilisateur(int idUtil, int idStatut)
        {
            
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                tbUtilisateur tb = mbdsEntity.tbUtilisateurs.Where(x => x.IdUtil == idUtil).FirstOrDefault<tbUtilisateur>();
                tb.Statut = idStatut;
                mbdsEntity.Entry(tb).Property("Statut").IsModified = true;
                mbdsEntity.SaveChanges();
            }

        }
    }
}
