﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbMonnaieDAOImpl : ITbMonnaieDAO<tbMonnaie>
    {
        public tbMonnaie Delete(tbMonnaie data)
        {
            throw new NotImplementedException();
        }

        public tbMonnaie FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbMonnaie> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbMonnaies.ToList<tbMonnaie>();
            }
        }

        public tbMonnaie Save(tbMonnaie data)
        {
            throw new NotImplementedException();
        }

        public tbMonnaie Update(tbMonnaie data)
        {
            throw new NotImplementedException();
        }
    }
}
