﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;
namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbHistProprieteDAOImpl : ITbHistProprieteDAO<tbHistPropriete>
    {
        public tbHistPropriete Delete(tbHistPropriete data)
        {
            throw new NotImplementedException();
        }

        public tbHistPropriete FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbHistPropriete> ListeAll()
        {
            throw new NotImplementedException();
        }

        public List<tbHistPropriete> ListeAllParPropriete(int idProp)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbHistProprietes
                    .Include(x=>x.tbProprietaire)
                    .Include(x=>x.tbProprietaire1)
                    .Where<tbHistPropriete>(x=>x.IdProp == idProp)
                    .ToList<tbHistPropriete>();
            }
        }

        public tbHistPropriete Save(tbHistPropriete data)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                 mbdsEntity.tbHistProprietes.Add(data);
                 mbdsEntity.SaveChanges();
                 return data;
            }
        }

        public tbHistPropriete Update(tbHistPropriete data)
        {
            throw new NotImplementedException();
        }
    }
}
