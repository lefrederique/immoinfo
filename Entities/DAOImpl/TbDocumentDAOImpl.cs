﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;
namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbDocumentDAOImpl : ITbDocumentDAO<tbDocument>
    {
        public tbDocument Delete(tbDocument data)
        {
            throw new NotImplementedException();
        }

        public tbDocument FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbDocument> ListeAll()
        {
            throw new NotImplementedException();
        }

        public List<tbDocument> ListeAllParPropriete(int idpro)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbDocuments
                    .Include(x => x.tbTypeDocument)
                    .Where<tbDocument>(x => x.Propriete==idpro)
                    .ToList<tbDocument>();
            }
        }

        public tbDocument Save(tbDocument data)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                mbdsEntity.tbDocuments.Add(data);
                mbdsEntity.SaveChanges();
                return data;
            }
        }

        public tbDocument Update(tbDocument data)
        {
            throw new NotImplementedException();
        }
    }
}
