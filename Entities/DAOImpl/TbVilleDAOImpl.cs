﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbVilleDAOImpl : ITbVilleDAO<tbVille>
    {
        MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities();
        public tbVille Delete(tbVille data)
        {
            throw new NotImplementedException();
        }

        public tbVille FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbVille> ListeAll()
        {
             return mbdsEntity.tbVilles.ToList< tbVille>();
        }

        public tbVille Save(tbVille data)
        {
            throw new NotImplementedException();
        }

        public tbVille Update(tbVille data)
        {
            throw new NotImplementedException();
        }

        public List<tbVille> ListeVilleParDepartement(int deptId)
        {
           return mbdsEntity.tbVilles.Where(x => x.tbDepartement.IdDept == deptId).ToList();
        }
    }
}
