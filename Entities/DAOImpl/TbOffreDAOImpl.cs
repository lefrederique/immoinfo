﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbOffreDAOImpl : ITbOffreDAO<tbOffre>
    {
        public tbOffre Delete(tbOffre data)
        {
            throw new NotImplementedException();
        }

        public tbOffre FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbOffre> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbOffres
                    .Include(x=>x.tbEnchere)
                    .Include(x=>x.tbProprietaire)
                    .ToList<tbOffre>();
            }
        }

        public tbOffre Save(tbOffre data)
        {

            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                //return mbdsEntity.tbOffres.ToList<tbOffre>();
            }

            return null;


        }

        public tbOffre Update(tbOffre data)
        {
            throw new NotImplementedException();
        }
    }
}
