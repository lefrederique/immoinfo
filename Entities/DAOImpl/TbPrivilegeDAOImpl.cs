﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbPrivilegeDAOImpl : ITbPrivilegeDAO<tbPrivilege>
    {
        public tbPrivilege Delete(tbPrivilege data)
        {
            throw new NotImplementedException();
        }

        public tbPrivilege FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbPrivilege> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbPrivileges.ToList<tbPrivilege>();
            }
        }

        public tbPrivilege Save(tbPrivilege data)
        {
            throw new NotImplementedException();
        }

        public tbPrivilege Update(tbPrivilege data)
        {
            throw new NotImplementedException();
        }
    }
}
