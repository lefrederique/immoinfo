﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbTypeProprietaireDAOImpl : ITbTypeProprietaireDAO<tbTypeProprietaire>
    {
        public tbTypeProprietaire Delete(tbTypeProprietaire data)
        {
            throw new NotImplementedException();
        }

        public tbTypeProprietaire FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbTypeProprietaire> ListeAll()
        {
            using(MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbTypeProprietaires.ToList<tbTypeProprietaire>();
            }
        }

        public tbTypeProprietaire Save(tbTypeProprietaire data)
        {
          

            throw new NotImplementedException();
        }

        public tbTypeProprietaire Update(tbTypeProprietaire data)
        {
            throw new NotImplementedException();
        }
    }
}
