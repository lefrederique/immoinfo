﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;
namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbStatutCompteDAOImpl : ITbStatutCompteDAO<tbStatutCompte>
    {
        public tbStatutCompte Delete(tbStatutCompte data)
        {
            throw new NotImplementedException();
        }

        public tbStatutCompte FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbStatutCompte> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbStatutComptes.ToList<tbStatutCompte>();
            }
        }

        public tbStatutCompte Save(tbStatutCompte data)
        {
            throw new NotImplementedException();
        }

        public tbStatutCompte Update(tbStatutCompte data)
        {
            throw new NotImplementedException();
        }
    }
}
