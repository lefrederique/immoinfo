﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;
using com.mbds.haiti.immoinfo.Entities.IMException;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbProprieteDAOImpl : ITbProprieteDAO<tbPropriete>
    {
        public tbPropriete Delete(tbPropriete data)
        {
            throw new NotImplementedException();
        }

        public tbPropriete FindOne(int Id)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {


                return mbdsEntity.tbProprietes
                    .Include(x => x.tbProprietaire)
                    .Include(x => x.tbMonnaie)
                    .Include(x => x.tbStatutPropriete)
                    .Include(x => x.tbVille)
                    .Where(x => x.IdProp == Id).FirstOrDefault<tbPropriete>();

            }
        }

        public List<tbPropriete> ListAllParProprietaire(int idProprietaire)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {


                return mbdsEntity.tbProprietes
                    .Include(x => x.tbProprietaire)
                    .Include(x => x.tbMonnaie)
                    .Include(x => x.tbStatutPropriete)
                    .Include(x => x.tbVille)
                    .Where(x => x.Proprietaire == idProprietaire).ToList<tbPropriete>();
                
            }
           
        }

        public List<tbPropriete> ListAllParLoginProprietaire(string login)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {


                return mbdsEntity.tbProprietes
                    .Include(x => x.tbProprietaire)
                    .Include(x => x.tbMonnaie)
                    .Include(x => x.tbStatutPropriete)
                    .Include(x => x.tbVille)
                    .Include(x => x.tbProprietaire.tbUtilisateur)
                    .Where(x => x.tbProprietaire.tbUtilisateur.Email == login).ToList<tbPropriete>();

            }

        }

        public List<tbPropriete> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbProprietes
                    .Include(x=>x.tbProprietaire)
                    .Include(x=>x.tbMonnaie)
                    .Include(x=>x.tbStatutPropriete)
                    .Include(x => x.tbVille)
                    .ToList<tbPropriete>();
            }
        }

        public tbEnchere MettreAuEncheres(decimal montantDepart, int propriete)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                int[] idstatut = new int[] { 1, 2 };
                tbPropriete tbp = mbdsEntity.tbProprietes.Where(x => x.Proprietaire == propriete && idstatut.Contains(x.Statut) ).FirstOrDefault<tbPropriete>();
                tbEnchere tbEnch = new tbEnchere();
                if (tbp!=null) {
                   
                    tbEnch.MontatDepart = montantDepart;
                    tbEnch.DateOuverture = DateTime.Now;
                    tbEnch.Statut = 1;
                    tbEnch.Propriete = propriete;
                    mbdsEntity.tbEncheres.Add(tbEnch);
                    mbdsEntity.SaveChanges();
                }
                else
                {

                    throw new ImmoAppException("Il y a deja un enchere en cours pour cette propriete", 200);
                }


                return tbEnch;
            }
        }

        public tbPropriete Save(tbPropriete data)
        {
           

            throw new NotImplementedException();
        }

        public tbPropriete Update(tbPropriete data)
        {
            throw new NotImplementedException();
        }
    }
}
