﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbDepartementDAOImpl : ITbDepartementDAO<tbDepartement>
    {
        MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities();
        public tbDepartement Delete(tbDepartement data)
        {
            throw new NotImplementedException();
        }

        public tbDepartement FindOne(int Id)
        {
            throw new NotImplementedException();
        }

        public List<tbDepartement> ListeAll()
        {
             return mbdsEntity.tbDepartements.ToList<tbDepartement>();
        }

        public tbDepartement Save(tbDepartement data)
        {
            throw new NotImplementedException();
        }

        public tbDepartement Update(tbDepartement data)
        {
            throw new NotImplementedException();
        }

       
    }
}
