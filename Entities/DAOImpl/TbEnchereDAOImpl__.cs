﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using com.mbds.haiti.immoinfo.Entities.IDAO;
using com.mbds.haiti.immoinfo.Entities.Models;

namespace com.mbds.haiti.immoinfo.Entities.DAOImpl
{
    public class TbEnchereDAOImpl__ : ITbEnchereDAO<tbEnchere>
    {
        public tbEnchere AnnulerEnchere(int idEnchere)
        {
            throw new NotImplementedException();
        }

        public tbEnchere CloturerEnchere(int idEnchere)
        {
            throw new NotImplementedException();
        }

        public tbEnchere Delete(tbEnchere data)
        {
            throw new NotImplementedException();
        }

        public tbEnchere FindOne(int Id)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbEncheres
                 .Include(x => x.tbStatutEnchere)
                 .Include(x => x.tbOffres)
                 .Where(x => x.IdEncheres == Id)
                 .FirstOrDefault<tbEnchere>();
            }
              
        }

        public List<tbEnchere> ListeAll()
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbEncheres
                    .Include(x => x.tbStatutEnchere)
                    .ToList<tbEnchere>();
            }
        }

        public List<tbEnchere> ListerEnchereParStatut(int idStatut)
        {
            using (MbdsImmoInfoEntities mbdsEntity = new MbdsImmoInfoEntities())
            {
                return mbdsEntity.tbEncheres
                 .Include(x => x.tbStatutEnchere)
                 .Include(x => x.tbOffres)
                 .Where(x => x.Statut == idStatut)
                 .ToList<tbEnchere>();
            }
        }

        public tbEnchere Save(tbEnchere data)
        {
            throw new NotImplementedException();
        }

        public tbEnchere Update(tbEnchere data)
        {
            throw new NotImplementedException();
        }

        public tbEnchere ValiderEnchere(int idEnchere)
        {
            throw new NotImplementedException();
        }
    }
}
